# MetaSnk: PhlAnProf
[![Snakemake](https://img.shields.io/badge/snakemake-≥5.4.4-brightgreen.svg)](https://snakemake.bitbucket.io)
[![Singularity](https://img.shields.io/badge/singularity-≥2.6.0-blue.svg)](https://sylabs.io/guides/3.5/user-guide/)
[![Build Status](https://travis-ci.org/snakemake-workflows/metagenomicsnake.svg?branch=master)](https://travis-ci.org/snakemake-workflows/metagenomicsnake)

## Description
PhlAnProf performs taxonmic and strain-level profiling using MetaPhlAn2 and StrainPhlAn. If pre-processing was not performed PhlAnProf will trigger execution of preQC.

<div style="text-align:center">
  <img src="./images/PhlAnProf_dag.png"  width="315" height="650" />
</div>

## Usage

### Case 1:
From within the MetaSnk directory (i.e where it was installed, where the Snakefile is located):

    snakemake --profile ./profiles/local --directory='path/to/workdir' -n PhlAnProf

--directory : specifies the working directory and it is where snakemake will store
              its files for tracking the status of the workflow before/during/after
              execution. You should change the working directory depending on your current working project.

After preQC is completed we can generate a html report:

    snakemake --directory='path/to/workdir' -n PhlAnProf_make_report

The report will be created in the path specified for OUT_DIR in the configuration file.

### Case2:
Execute preQC from a working directory outside the MetaSnk directory:

    snakemake --profile $metasnk/profiles/local -s $metasnk/Snakefile -n PhlAnProf
