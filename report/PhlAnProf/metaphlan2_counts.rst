Table with MetaPhlAn2 taxonomic profiles of all samples in dataset
"{{ snakemake.wildcards.dataset }}". Values are MetaPhlAn2's estimated counts.
