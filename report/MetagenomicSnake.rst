Workflow version 0.3

MetaSnk
================
MetaSnk is a Snakemake workflow for the analysis of metagenomic
datasets from human microbiomes.

----

Modules
-------
**rawQC**
  runs FastQC on a random sample of R1 reads from the paired fastq-format files.

**preQC**
  FastQC only performs a quality check but no QC processing is done. The **preQC**
  rule runs a multi-step pre-processing of the paired fastq files, it includes:

  - **trim_adapters**: adapter-trimming with "fastp". Fastp performs a quality check
    and both paired fastq files are processed as follows\:

      + remove adapters: here we provide the Nextera XT adapters,
      + base correction in overlapped regions
      + trimming of the last base in read 1
      + discard reads shorter than a minimum length, after trimming
      + a report with quality check, before and after processing
  - **filter_human**: removal of reads derived from human DNA with BBTools' bbsplit_
  - **dedupe**: removal of duplicated reads with BBTools' clumpify_
  - **trim_3end**: 3\'-end quality trimming with "fastp"
  - **concatenate_fastqs**: merges fastq files corresponding to the same sample into a single pair of fastq files
  - **summarize_preQC**: creates summarizing tables and plots

**PhlAnProf**
    performs taxonmic and strain-level profiling using MetaPhlAn2 and StrainPhlAn.
    If pre-processing was not performed PhlAnProf will trigger execution of preQC.

**HUMAnN2Prof**
    performs gene- and pathway-level functional profiling using HUMAnN2. If pre-processing(preQC)
    and taxonomic profiling with MetaPhlAn2 was not performed it will trigger their execution.

.. _bbsplit: http://seqanswers.com/forums/showthread.php?t=41288
.. _clumpify: https://jgi.doe.gov/data-and-tools/bbtools/bb-tools-user-guide/clumpify-guide/
