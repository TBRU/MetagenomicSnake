Bar plot showing the proportion of sequencing units, in dataset "{{ snakemake.wildcards.dataset }}",
by their status (pass, warning, fail) for each FastQC test.
