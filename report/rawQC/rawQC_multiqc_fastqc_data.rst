Table summarizing FastQC results for all R1 fastq files in dataset
"{{ snakemake.wildcards.dataset }}".

{% if snakemake.config['rawQC']['samplerate'] < 1 -%} Notice that only
a fraction ({{ snakemake.config['rawQC']['samplerate'] }}) of reads were
analyzed per fastq file. {% else -%} {%- endif %}
