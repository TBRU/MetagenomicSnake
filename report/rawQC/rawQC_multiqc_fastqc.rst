Quality control report for dataset "{{ snakemake.wildcards.dataset }}" generated
with MultiQC after parsing all FastQC reports generated per each R1 fastq file.

{% if snakemake.config['rawQC']['samplerate'] < 1 -%} Notice that only
a fraction ({{ snakemake.config['rawQC']['samplerate'] }}) of reads were
analyzed per fastq file. {% else -%} {%- endif %}
