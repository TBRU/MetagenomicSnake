Histogram plot showing the distribution of 'Total Pairs' per sample in dataset
"{{ snakemake.wildcards.dataset }}". Sequencing units (paired fastq files) in
dataset "{{ snakemake.wildcards.dataset }}" were merged by 'Sample' ID.
