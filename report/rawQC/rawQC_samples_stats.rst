Table with summary statistics for the amount of read pairs for each sample. Sequencing
units (paired fastq files) in dataset "{{ snakemake.wildcards.dataset }}" were
merged by 'Sample' ID. To have a general idea about the dispersion of sequencing
data obtained per sample, this table includes the coefficient of range (c.r):

.. math::
  \frac{ Xi_{max} - Xi_{min} }{ Xi_{max} + Xi_{min} }


Where:

  - Xi\ :sub:`max`: maximum number of read pairs in a sequencing unit for sample i
  - Xi\ :sub:`min`: minimum number of read pairs in a sequencing unit for sample i

{% if snakemake.config['rawQC']['samplerate'] < 1 -%} Notice that only
a fraction ({{ snakemake.config['rawQC']['samplerate'] }}) of reads were
analyzed per fastq file, but 'Total Pairs' were estimated. {% else -%} {%- endif %}
