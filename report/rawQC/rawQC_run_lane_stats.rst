Table with summary statistics for the amount of read pairs for each sequencing
lane in a sequencing run. To have a general idea about the dispersion of sequencing
data obtained per 'Lane', this table includes the coefficient of range (c.r).

{% if snakemake.config['rawQC']['samplerate'] < 1 -%} Notice that only
a fraction ({{ snakemake.config['rawQC']['samplerate'] }}) of reads were
analyzed per fastq file, but 'Total Pairs'per fastq file were estimated.
{% else -%} {%- endif %}
