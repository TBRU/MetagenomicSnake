Sequencing units (paired fastq files) in dataset "{{ snakemake.wildcards.dataset }}"
were merged by 'Sample' ID. To have a general idea about the dispersion of sequencing
data (total read pairs per sequencing unit) obtained per sample, this plot shows
an histogram of the coefficient of range (c.r) of each sample.
