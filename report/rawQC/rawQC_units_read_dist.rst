Histogram plot showing the distribution of 'Total Pairs' per sequencing
unit in dataset "{{ snakemake.wildcards.dataset }}".
