#!/bin/bash
# properties = {properties}
echo "Job information:"
echo "--mem $SLURM_MEM_PER_NODE"
echo "--ntasks $SLURM_NTASKS"
echo "times restarted: $SLURM_RESTART_COUNT"
echo "Node name: $SLURMD_NODENAME"

module load Singularity/2.6.1
source activate MetaSnk

{exec_job}
