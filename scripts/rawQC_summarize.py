import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
from contextlib import redirect_stderr

def get_rawqc_stats(fastqc_data_path, sampling):
    multiqc_fastqc_data_file = fastqc_data_path
    try:
        multiqc_fastqc_data = pd.read_csv(multiqc_fastqc_data_file, sep='\t', header=0, index_col=0)
    except OSError.filename:
        print('Unable to load {}! Check that it exists!'.format(multiqc_fastqc_data))
        exit
    sample_run_lane = pd.DataFrame.from_dict({unit:{'Sample':unit.split('-')[0],
                                                    'Run':unit.split('-')[1].split('_')[0],
                                                    'Lane':unit.split('-')[1].split('_')[1]
                                                   }
                                              for unit in multiqc_fastqc_data.index
                                             },
                                             orient='index'
                                            )
    sampling_rate = sampling
    sampling_multiplier = 1/sampling_rate
    sample_run_lane['Sampling'] = sampling_rate
    sample_run_lane['Total Pairs'] = [multiqc_fastqc_data.loc[unit,'Total Sequences']*sampling_multiplier
                                      for unit in sample_run_lane.index]

    fastc_percent_fails = pd.DataFrame.from_dict({index:(np.sum(row=='fail')/len(row))*100 for index,
                                                  row in multiqc_fastqc_data.iloc[:,9:].iterrows()},
                                                 orient='index',
                                                 columns=['fastqc_percent_fails'])
    sample_run_lane = pd.concat([sample_run_lane,fastc_percent_fails], axis=1)
    return(sample_run_lane)

def get_sample_stats(rawqc_stats):
    sample_run_lane = rawqc_stats
    per_sample_summary = sample_run_lane[['Sample','Total Pairs']].groupby('Sample').describe()['Total Pairs']
    per_sample_summary['range'] = per_sample_summary['max'] - per_sample_summary['min']
    per_sample_summary['c.r'] = (per_sample_summary['max'] - per_sample_summary['min'])/(per_sample_summary['max'] + per_sample_summary['min'])
    per_sample_summary['Total Pairs'] = per_sample_summary['mean']*per_sample_summary['count']
    return(per_sample_summary)

def get_run_lane_stats(rawqc_stats):
    sample_run_lane = rawqc_stats
    per_run_lane_summary = sample_run_lane[['Run','Lane','Total Pairs']].groupby(['Run','Lane']).describe()['Total Pairs']
    per_run_lane_summary['range'] = per_run_lane_summary['max'] - per_run_lane_summary['min']
    per_run_lane_summary['c.r'] = (per_run_lane_summary['max'] - per_run_lane_summary['min'])/(per_run_lane_summary['max'] + per_run_lane_summary['min'])
    return(per_run_lane_summary)

def totalpairs_formatter(x, pos):
    """The two args are the value and tick position"""
    if x >= 1e6:
        s = '${:1.1f}M'.format(x*1e-6)
    else:
        s = '${:1.0f}K'.format(x*1e-3)
    return s

def plot_total_pairs_dist(data, ax, xaxis_formatter=totalpairs_formatter):
    ax.hist(data, bins='scott', color='#91bfdb')
    ax.set(xlabel='Total Pairs', ylabel='Frequency',
    title='Total Read Pairs of sequencing units')
    formatter = FuncFormatter(xaxis_formatter)
    ax.xaxis.set_major_formatter(formatter)

def plot_fastqc_status(fastqc_data_path, ax):
    multiqc_fastqc_data_file = fastqc_data_path
    try:
        multiqc_fastqc_data = pd.read_csv(multiqc_fastqc_data_file, sep='\t', header=0, index_col=0)
    except OSError.filename:
        print('Unable to load {}! Check that it exists!'.format(multiqc_fastqc_data))
        exit
    multiqc_fastqc_data_stacked = pd.DataFrame(list(multiqc_fastqc_data.iloc[:,10:].stack().index),
                                  columns=['unit','FastQC Test'])
    multiqc_fastqc_data_stacked['status'] = list(multiqc_fastqc_data.iloc[:,10:].stack())
    multiqc_fastqc_data_crosstab = pd.crosstab(multiqc_fastqc_data_stacked['FastQC Test'],
                                   multiqc_fastqc_data_stacked.status, normalize='index')
    multiqc_fastqc_data_crosstab.loc[:,['pass','warn','fail']].plot.barh(stacked=True,
    ax=ax, legend=False, color=['#91bfdb','#ffffbf','#fc8d59'])
    ax.legend(bbox_to_anchor=(1, 0.7), title='Status')
    ax.set(xlabel='Sequencing units (fraction)',title='Status of FasQC tests')

def main():
    with open(snakemake.log[0], 'w') as stderr, redirect_stderr(stderr):
        multiqc_fastqc_data_file = snakemake.input[0]

        # Summary per sequencing unit (sample-run_lane)
        sample_run_lane = get_rawqc_stats(multiqc_fastqc_data_file, sampling=snakemake.config['rawQC']['samplerate'])
        units_summary_out_file = snakemake.output['units_stats']
        sample_run_lane.to_csv(units_summary_out_file, sep='\t', index_label='Unit')

        # Summary per sample
        per_sample_summary = get_sample_stats(sample_run_lane)
        sample_summary_out_file = snakemake.output['samples_stats']
        per_sample_summary.to_csv(sample_summary_out_file, sep='\t', index_label='Sample')

        # Summary per run&Lane combination
        per_run_lane_summary = get_run_lane_stats(sample_run_lane)
        run_lane_stats_file = snakemake.output['run_lane_stats']
        per_run_lane_summary.to_csv(run_lane_stats_file, sep='\t', index_label=['Run', 'Lane'])

        # Plot distribution of per-unit total number of read pairs
        per_unit_read_pairs_fig_file = snakemake.output['units_read_pairs_plot']
        plt.style.use('seaborn-whitegrid')
        plt.rcParams.update({'figure.autolayout': True})
        fig1, ax1 = plt.subplots()
        plot_total_pairs_dist(data=sample_run_lane['Total Pairs'], ax=ax1)
        ax1.set(ylabel='Śequencing units')
        fig1.tight_layout()
        fig1.savefig(per_unit_read_pairs_fig_file, bbox_inches='tight', format='svg')

        # Plot distribution of per-sample total number of read pairs
        per_sample_read_pairs_fig_file = snakemake.output['samples_read_pairs_plot']
        fig2, ax2 = plt.subplots()
        plot_total_pairs_dist(data=per_sample_summary['Total Pairs'], ax=ax2)
        ax2.set(ylabel='Samples')
        fig2.tight_layout()
        fig2.savefig(per_sample_read_pairs_fig_file, bbox_inches='tight', format='svg')

        # Plot co-efficients of dispersion for number of total read pairs per sample
        per_sample_coef_range_fig_file = snakemake.output['samples_read_pairs_dispersion_plot']
        fig3, ax3 = plt.subplots()
        ax3.hist(per_sample_summary['c.r'], bins='scott', color='#91bfdb')
        ax3.set(xlabel='Co-efficient of range (c.r)', ylabel='Samples',
        title='Co-efficient of range of total read pairs per sample')
        fig3.tight_layout()
        fig3.savefig(per_sample_coef_range_fig_file, bbox_inches='tight', format='svg')

        # Plot status of FatQC tests
        fastqc_tests_status_file = snakemake.output['fastqc_tests_status_plot']
        fig4, ax4 = plt.subplots()
        plot_fastqc_status(multiqc_fastqc_data_file, ax=ax4)
        fig4.tight_layout()
        fig4.savefig(fastqc_tests_status_file, bbox_inches='tight', format='svg')
if __name__ == "__main__":
    main()
