# The main entry point of your workflow.
# After configuring, running snakemake -n in a clone of this repository should
# successfully execute a dry-run of the workflow.

report: "report/MetagenomicSnake.rst"

import os
##----------------------------------------------------------------------------##
## Check available resources
##----------------------------------------------------------------------------##
from multiprocessing import cpu_count
cpus_avail = cpu_count()
print('CPUs available: {}'.format(cpus_avail), file=sys.stderr)

##----------------------------------------------------------------------------##
## Check existence of expected variables in the environment
##----------------------------------------------------------------------------##
try:
    print('MetaSnk will store reference data/databases at {}'.format(os.environ["METASNK_DBS"]),
          file=sys.stderr)
except KeyError:
    print("You did not set variable METASNK_DBS", file=sys.stderr)
    exit(1)

##----------------------------------------------------------------------------##
## Re-set working directory
##----------------------------------------------------------------------------##
# Set working directory
workdir_path = os.path.dirname(os.path.abspath(__name__))
workflow_path = workflow.basedir

if workdir_path == workflow_path:
    message = "Working directory was not specified!"+\
              "Then, MetagenomicSnake assumes ./data, relative to current directory, "+\
              "as your working directory ..."
    print(message, file=sys.stderr)
    if os.path.exists(workflow_path+'/data'):
        workdir_path = workflow_path+'/data'
        workdir: workdir_path
        print("Working directory:{}".format(workdir_path), file=sys.stderr)
    else:
        message = "... Folder ./data not found in current directory..."+\
                  "... instead, setting current directory as working directory ..."+\
                  "Working directory:{}".format(workdir_path)
        print(message, file=sys.stderr)
else:
    print("Working directory:{}".format(workdir_path), file=sys.stderr)

##----------------------------------------------------------------------------##
## Configuration of MetaSnK
##----------------------------------------------------------------------------##
try:
    config['RAW_DIR']
    #configfile_path = config['configfile_path']
    print("Configuration file provided at execution!", file=sys.stderr)
except:
    print("Configuration file config.yaml not especified at execution!", file=sys.stderr)
    try:
        print("... Trying working directory ...", file=sys.stderr)
        configfile_path = "config.yaml"
        configfile: configfile_path
        print("... Configuration file: {}".format(configfile_path), file=sys.stderr)
    except:
        message = "... config.yaml not found in working directory ..."+\
                  "... Loading default config.yaml provided with MetagenomicSnake..."
        print(message, file=sys.stderr)
        configfile_path = workflow_path + "/config.yaml"
        configfile: configfile_path
        print("... Configuration file: {}".format(configfile_path), file=sys.stderr)
finally:
    print("Provided configuration for MetaSnk:", file=sys.stderr)
    my_temp=[print("    {}:{}\n".format(key,value), file=sys.stderr) for key,value in config.items()]

##----------------------------------------------------------------------------##
## Define paths and local variables
##----------------------------------------------------------------------------##
print("MetaSnk is setting local variables ...")

try:
    print("Setting RAW_DIR")
    if os.path.exists(config['RAW_DIR'])==False:
        raise FileNotFoundError
    else:
        RAW_FASTQ_DIR = config['RAW_DIR']
        print("RAW_DIR: {}".format(RAW_FASTQ_DIR), file=sys.stderr)
except FileNotFoundError:
    print("  Provided path does not exist!\n"+\
          "  Please provide an absolute path for RAW_DIR in your configuration file!")
    exit(1)

try:
    print("Setting OUT_DIR")
    if os.path.exists(config['OUT_DIR'])==False:
        raise FileNotFoundError
    else:
        OUT_DIR = config['OUT_DIR']
        print("OUT_DIR: {}".format(OUT_DIR), file=sys.stderr)
except FileNotFoundError:
    print("  Provided path does not exist!\n"+\
          "  Please provide absolute path for OUT_DIR in your configuration file!")
    exit(1)
OUT_DIR = config['OUT_DIR']

METASNK_DB_DIR = os.environ["METASNK_DBS"]
SHUB_PREQC_SIF = 'shub://mticlla/MetagenomicSnake:preqc_v0_1'
SHUB_METAPROF_SIF = 'shub://mticlla/OmicSingularities:metaprof'
PREQC_SIF = METASNK_DB_DIR+'/singularity/MetagenomicSnake_preqc_v0_1.sif'
METAPROF_SIF = METASNK_DB_DIR+'/singularity/OmicSingularities_metaprof.sif'

HUMAN_REF = eval(config['preprocess']['filter_human']['bbmap_reference'])
print("HUMAN_REF: {}".format(HUMAN_REF), file=sys.stderr)
##----------------------------------------------------------------------------##
## Fastq files to be processed
##----------------------------------------------------------------------------##
if config['SAMPLE_UNITS']['auto']:
    (DATASETS, SAMPLES, RUNS, LANES) = glob_wildcards(RAW_FASTQ_DIR+'/{dataset}/fastq/{sample}-{run}_{lane}-R1.fastq.gz')
    (DATASETSX, FASTQS) = glob_wildcards(RAW_FASTQ_DIR+'/{dataset}/fastq/{fastq_file}-R1.fastq.gz')
    DATASETS_SAMPLES_DICT = dict()
    for dataset in set(DATASETS):
        samples = list({SAMPLES[ix] for ix,value in enumerate(DATASETS) if value==dataset})
        DATASETS_SAMPLES_DICT[dataset] = samples
else:
    # TODO:
    pass
##----------------------------------------------------------------------------##
## Local rules
##----------------------------------------------------------------------------##
localrules:
    pullSIFS,
    buildDBS,
    rawQC,
    preQC,
    MetaPhlAn2,
    StrainPhlAn,
    PhlAnProf,
    HUMAnN2Prof,
    rawQC_make_report,
    preQC_make_report,
    MetaPhlAn2_make_report,
    PhlAnProf_make_report,
    HUMAnN2Prof_make_report
##----------------------------------------------------------------------------##
## Run entire workflow
##----------------------------------------------------------------------------##
rule all:
    input:
        # The first rule should define the default target files
        # Subsequent target rules can be specified below. They should start with
        # all_*.

##----------------------------------------------------------------------------##
## Modules
##----------------------------------------------------------------------------##
rule pullSIFS:
    input:
        PREQC_SIF,
        METAPROF_SIF

rule buildDBS:
    input:
        METASNK_DB_DIR+'/strainphlan_markers/all_markers.fasta',
        METASNK_DB_DIR+'/humann2_databases/chocophlan',
        METASNK_DB_DIR+'/humann2_databases/uniref',
        METASNK_DB_DIR+'/humann2_databases/utility_mapping',
        HUMAN_REF
        #METASNK_DB_DIR+'/ref_genomes/hg19_main_mask_ribo_animal_allplant_allfungus.fa.gz'

rule rawQC:
    input:
        expand(OUT_DIR + '/{dataset}/rawQC/summary_stats/{dataset}_samples_stats.txt', dataset=set(DATASETS)),
        expand(OUT_DIR + '/{dataset}/rawQC/summary_stats/{dataset}_units_stats.txt', dataset=set(DATASETS)),
        expand(OUT_DIR + '/{dataset}/rawQC/summary_stats/{dataset}_run_lane_stats.txt', dataset=set(DATASETS)),
        expand(OUT_DIR + '/{dataset}/rawQC/summary_stats/{dataset}_units_read_dist.svg', dataset=set(DATASETS)),
        expand(OUT_DIR + '/{dataset}/rawQC/summary_stats/{dataset}_samples_read_dist.svg', dataset=set(DATASETS)),
        expand(OUT_DIR + '/{dataset}/rawQC/summary_stats/{dataset}_samples_read_dispersion.svg', dataset=set(DATASETS)),
        expand(OUT_DIR + '/{dataset}/rawQC/summary_stats/{dataset}_fastqc_tests_status.svg', dataset=set(DATASETS))

rule preQC:
    input:
        expand(OUT_DIR + '/{dataset}/preQC/summary_stats/{dataset}_preqc_units_summary.tsv', dataset=set(DATASETS)),
        expand(OUT_DIR + '/{dataset}/preQC/summary_stats/{dataset}_preqc_samples_summary.tsv', dataset=set(DATASETS)),
        expand(OUT_DIR + '/{dataset}/preQC/summary_stats/{dataset}_preqc_units_barchart.svg', dataset=set(DATASETS)),
        expand(OUT_DIR + '/{dataset}/preQC/summary_stats/{dataset}_preqc_samples_barchart.svg', dataset=set(DATASETS)),
        expand(OUT_DIR + '/{dataset}/preQC/summary_stats/{dataset}_preqc_units_pct_barchart.svg', dataset=set(DATASETS)),
        expand(OUT_DIR + '/{dataset}/preQC/summary_stats/{dataset}_preqc_samples_pct_barchart.svg', dataset=set(DATASETS)),
        expand(OUT_DIR + '/{dataset}/preQC/summary_stats/{dataset}_concatenation_all.done', dataset=set(DATASETS))
rule MetaPhlAn2:
    input:
        expand(OUT_DIR + '/{dataset}/PhlAnProf/metaphlan/profiles_merged/{dataset}_abundances_sp_heatmap.png', dataset=set(DATASETS))
rule StrainPhlAn:
    input:
        expand(OUT_DIR + '/{dataset}/PhlAnProf/strphlan/{dataset}_clades_profiled.tsv', dataset=set(DATASETS))
rule PhlAnProf:
    input:
        expand(OUT_DIR + '/{dataset}/PhlAnProf/metaphlan/profiles_merged/{dataset}_abundances_sp_heatmap.png', dataset=set(DATASETS)),
        expand(OUT_DIR + '/{dataset}/PhlAnProf/strphlan/{dataset}_clades_profiled.tsv', dataset=set(DATASETS))
rule HUMAnN2Prof:
    input:
        expand(OUT_DIR + '/{dataset}/Humann2/all.done', dataset=set(DATASETS))

##----------------------------------------------------------------------------##
## Rules to make reports
##----------------------------------------------------------------------------##
rule rawQC_make_report:
    output:
        temp(touch(OUT_DIR+'/logs/raw_QC_make_report.done'))
    log:
        OUT_DIR+'/logs/rawQC_make_report.log'
    params:
        report_path = OUT_DIR+'/rawQC_report.html',
        workdir = workdir_path,
        workflow_dir = workflow_path
    shell:
        '''
        (snakemake \
        --directory={params.workdir} \
        --report {params.report_path} \
        -s {params.workflow_dir}/Snakefile rawQC) &>{log}
        '''

rule preQC_make_report:
    output:
        temp(touch(OUT_DIR+'/logs/preQC_make_report.done'))
    log:
        OUT_DIR+'/logs/preQC_make_report.log'
    params:
        report_path = OUT_DIR+'/preQC_report.html',
        workdir = workdir_path,
        workflow_dir = workflow_path
    shell:
        '''
        (snakemake \
        --directory={params.workdir} \
        --report {params.report_path} \
        -s {params.workflow_dir}/Snakefile preQC) &>{log}
        '''

rule MetaPhlAn2_make_report:
    output:
        temp(touch(OUT_DIR+'/logs/metaphlan2_make_report.done'))
    log:
        OUT_DIR+'/logs/MetaPhlAn2_make_report.log'
    params:
        report_path = OUT_DIR+'/MetaPhlAn2_report.html',
        workdir = workdir_path,
        workflow_dir = workflow_path
    shell:
        '''
        (snakemake \
        --directory={params.workdir} \
        --report {params.report_path} \
        -s {params.workflow_dir}/Snakefile MetaPhlAn2) &>{log}
        '''

rule PhlAnProf_make_report:
    output:
        temp(touch(OUT_DIR+'/logs/phlanprof_make_report.done'))
    log:
        OUT_DIR+'/logs/PhlAnProf_make_report.log'
    params:
        report_path = OUT_DIR+'/PhlAnProf_report.html',
        workdir = workdir_path,
        workflow_dir = workflow_path
    shell:
        '''
        (snakemake \
        --directory={params.workdir} \
        --report {params.report_path} \
        -s {params.workflow_dir}/Snakefile PhlAnProf) &>{log}
        '''
rule HUMAnN2Prof_make_report:
    output:
        temp(touch(OUT_DIR+'/logs/humann2prof_make_report.done'))
    log:
        OUT_DIR+'/logs/humann2prof_make_report.log'
    params:
        report_path = OUT_DIR+'/HUMAnN2Prof_report.html',
        workdir = workdir_path,
        workflow_dir = workflow_path
    shell:
        '''
        (snakemake \
        --directory={params.workdir} \
        --report {params.report_path} \
        -s {params.workflow_dir}/Snakefile HUMAnN2Prof) &>{log}
        '''

include: "rules/setup_rules.smk"
include: "rules/rawQC.smk"
include: "rules/preprocess.smk"
include: "rules/phlanprof.smk"
include: "rules/humann2.smk"
