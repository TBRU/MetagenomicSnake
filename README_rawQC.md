# MetaSnk: rawQC

## Description
It runs FastQC on a random sample of R1 reads from the paired fastq-format files. MultiQC generates a single report per dataset.

rawQC is an independent module, its output files are not required as inputs in other MetaSnk rules.

<div style="text-align:center">
  <img src="./images/rawQC_dag.png"  width="130" height="250" />
</div>

## Usage

### Case 1:
From within the MetaSnk directory (i.e where it was installed, where the Sbakefile is located):

    snakemake --profile ./profiles/local --directory='path/to/workdir' -n rawQC

--directory : specifies the working directory and it is where snakemake will store its files for tracking the status of the workflow before/during/after execution.

After rawQC is completed we can generate a html report:

    snakemake --directory='path/to/workdir' -n rawQC_make_report

The report will be created in the path specified for OUT_DIR in the configuration file.

### Case2:
Execute rawQC from a working directory outside the MetaSnk directory:

    snakemake --profile $metasnk/profiles/local -s $metasnk/Snakefile -n rawQC
