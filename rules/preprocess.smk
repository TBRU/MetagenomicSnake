'''
Author: Monica R. Ticlla
Afiliation(s): SIB, SwissTPH, UNIBAS
Description: rules for pre-processing of paired-end shotgun DNA metagenomic
sequencing.
'''

##----------------------------------------------------------------------------##
## Import modules
##----------------------------------------------------------------------------##
#from utils import summarize_filter_human_step

##----------------------------------------------------------------------------##
## Local rules
##----------------------------------------------------------------------------##
localrules:
    check_concatenation,
    mqc_trim_adapters_list_files,
    mqc_filter_human_list_files,
    mqc_trim_3end_list_files,
    mqc_trim_adapters,
    mqc_filter_human,
    multiqc_trim_3end,
    summarize_preQC
##----------------------------------------------------------------------------##
## Local variables
##----------------------------------------------------------------------------##
singularity_preqc = PREQC_SIF
BBMAP_REF = HUMAN_REF
BBMAP_REF_DIR = eval(config['preprocess']['filter_human']['bbmap_ref_dir'])
##----------------------------------------------------------------------------##
## Helper functions
##----------------------------------------------------------------------------##
# Helper functions to create list of files for MultiQC

def mqc_trim_adapters_inputs(wildcards):
    list_files = ['{}/{}/preQC/atrimmed/{}.fastp.json'.format(OUT_DIR,
                                                            value,
                                                            FASTQS[ix])
                for ix,value in enumerate(DATASETSX)
                if value==wildcards.dataset]
    return(list_files)

def mqc_filter_human_inputs(wildcards):
    list_files = ['{}/{}/preQC/bfiltered/{}.clean.stats'.format(OUT_DIR,
                                                            value,
                                                            FASTQS[ix])
                for ix,value in enumerate(DATASETSX)
                if value==wildcards.dataset]
    return(list_files)

def mqc_trim_3end_inputs(wildcards):
    list_files = ['{}/{}/preQC/dfinaltrim/{}.clean.nodup.fastp.json'.format(OUT_DIR,
                                                                            value,
                                                                            FASTQS[ix])
                  for ix,value in enumerate(DATASETSX)
                  if value==wildcards.dataset]
    return(list_files)

def list_concatenated_r1_fastqs(wildcards):
    list_r1 = ['{}/{}/preQC/emerged/{}-R1.fastq.gz'.format(OUT_DIR,
                                                          wildcards.dataset,
                                                          value)
               for ix,value in enumerate(SAMPLES) if DATASETS[ix]==wildcards.dataset]
    return(list_r1)
def list_concatenated_r2_fastqs(wildcards):
    list_r2 = ['{}/{}/preQC/emerged/{}-R2.fastq.gz'.format(OUT_DIR,
                                                          wildcards.dataset,
                                                          value)
               for ix,value in enumerate(SAMPLES) if DATASETS[ix]==wildcards.dataset]
    return(list_r2)
##----------------------------------------------------------------------------##
## Rules with target files
##----------------------------------------------------------------------------##

# FastQC only performs a quality check but no QC processing is done.
# With Fastp a quality check is performed and both paired fastq files
# are processed as follows:
#
# - default fastp's quality filtering by:
#   - limiting the N gase number (-n)
#   - percentage of unqualified bases (-u)
#   - average quality score (-e, default 0 means no requirement)
# - remove adapters: enabled by default, fastp detects adapters by
#   per-read overlap analysis (seeks for the overlap of each read pair).
#   If fastp fails to find an overlap, It usess the provided the adapter
#   sequences (e.g Nextera XT adapters).
#   Check this page from Illumina to know which adapters to remove:
#   https://support.illumina.com/bulletins/2016/12/what-sequences-do-i-use-for-adapter-trimming.html
# - base correction in overlapped regions
# - trimming of the last base(s) for every read pair(this step preceeds
#   adapter removal)
# - length filtering: discard reads shorter than a minimum length, after trimming
#
# WARNING: cutting by quality score is not done at this stage because it
# interferes with deduplication step (rule dedupe).
#
# FastP provides a report with quality check, before and after processing
rule trim_adapters:
    input:
        fwd = lambda wildcards: ['{}/{}/fastq/{}-R1.fastq.gz'.format(RAW_FASTQ_DIR, DATASETSX[ix], value)
        for ix,value in enumerate(FASTQS) if value==wildcards.fastq_file],
        rev = lambda wildcards: ['{}/{}/fastq/{}-R2.fastq.gz'.format(RAW_FASTQ_DIR, DATASETSX[ix], value)
        for ix,value in enumerate(FASTQS) if value==wildcards.fastq_file]
    output:
        fwd_tr = temp(OUT_DIR+'/{dataset}/preQC/atrimmed/{fastq_file}-R1.fastp.fastq.gz'),
        rev_tr = temp(OUT_DIR+'/{dataset}/preQC/atrimmed/{fastq_file}-R2.fastp.fastq.gz'),
        report1 = OUT_DIR+'/{dataset}/preQC/atrimmed/{fastq_file}.fastp.html',
        report2 = OUT_DIR+'/{dataset}/preQC/atrimmed/{fastq_file}.fastp.json'
    log:
        OUT_DIR + '/{dataset}/preQC/logs/atrimmed/{fastq_file}.log'
    params:
        #fastp_dir = PRE_PROC_DIR,
        adapter = config['preprocess']['trim_adapters']['adapter'],
        min_length = config['preprocess']['trim_adapters']['min_length'],
        trim_tails = config['preprocess']['trim_adapters']['trim_tails']
    threads: cpus_avail
    singularity: singularity_preqc
    group: 'preprocess'
    message: "Running trim_adapters with {threads} cores."
    shell:
        '''
        (fastp \
        --adapter_sequence {params.adapter} \
        --adapter_sequence_r2 {params.adapter} \
        --detect_adapter_for_pe \
        --correction \
        --length_required {params.min_length} \
        --trim_tail1 {params.trim_tails} \
        --trim_tail2 {params.trim_tails} \
        --in1 {input.fwd} --in2 {input.rev} \
        --out1 {output.fwd_tr} --out2 {output.rev_tr} \
        --html {output.report1} --json {output.report2} \
        --thread {threads}) &>{log}
        '''
# After pre-processing with fastp, human reads have to be removed
#
rule filter_human:
    input:
        human_ref = BBMAP_REF_DIR,
        fwd_tr = OUT_DIR+'/{dataset}/preQC/atrimmed/{fastq_file}-R1.fastp.fastq.gz',
        rev_tr = OUT_DIR+'/{dataset}/preQC/atrimmed/{fastq_file}-R2.fastp.fastq.gz',
    output:
        fwd_clean = temp(OUT_DIR+'/{dataset}/preQC/bfiltered/{fastq_file}-R1.clean.fastq.gz'),
        rev_clean = temp(OUT_DIR+'/{dataset}/preQC/bfiltered/{fastq_file}-R2.clean.fastq.gz'),
        bbmap_scafstats = OUT_DIR+'/{dataset}/preQC/bfiltered/{fastq_file}.clean.scafstats',
        bbmap_refstats = OUT_DIR+'/{dataset}/preQC/bfiltered/{fastq_file}.clean.stats'
    log:
        OUT_DIR + '/{dataset}/preQC/logs/bfiltered/{fastq_file}.log'
    params:
        low_mem = config['preprocess']['filter_human']['bbmap_usemodulo'],
        mem_gb = config['preprocess']['filter_human']['bbmap_mem']
    resources:
        # in minutes
        runtime = lambda wildcards, attempt: 120*attempt
    threads:cpus_avail
    singularity: singularity_preqc
    group: 'preprocess'
    message: "Running filter_human with {threads} cores."
    shell:
        '''
        (bbsplit.sh \
        usemodulo={params.low_mem} \
        minid=0.95 \
        maxindel=3 \
        bwr=0.16 \
        bw=12 \
        minhits=2 \
        quickmatch \
        fast \
        qtrim=rl \
        trimq=10 \
        untrim \
        path={input.human_ref} \
        in1={input.fwd_tr} in2={input.rev_tr} \
        outu1={output.fwd_clean} outu2={output.rev_clean} \
        sortscafs=t \
        scafstats={output.bbmap_scafstats} \
        refstats={output.bbmap_refstats} \
        unpigz=t pigz=t -Xmx{params.mem_gb}g \
        threads={threads}) &>{log}
        '''

# This is an intensive job
#
rule index_human_ref:
    input:
        # Masked hg19 kindly provided by Brian Brushnell and
        # dowloaded from
        # https://drive.google.com/open?id=0B3llHR93L14wd0pSSnFULUlhcUk
        reference = BBMAP_REF
    output:
        directory(BBMAP_REF_DIR)
    log:
        OUT_DIR + '/logs/ref_indexing.log'
    params:
        usemodulo=config['preprocess']['filter_human']['bbmap_usemodulo'],
        mem_gb = config['preprocess']['filter_human']['bbmap_mem']
    threads: cpus_avail
    singularity: singularity_preqc
    message: "Running index_human_ref with {threads} cores."
    shell:
        '''
        (bbsplit.sh \
        -Xmx{params.mem_gb}g \
        ref={input.reference} \
        usemodulo={params.usemodulo} \
        path={output} \
        threads={threads}) &>{log}
        '''
# Deduplication step with BBTool dedupe.sh
rule dedupe:
    input:
        fwd_clean = OUT_DIR+'/{dataset}/preQC/bfiltered/{fastq_file}-R1.clean.fastq.gz',
        rev_clean = OUT_DIR+'/{dataset}/preQC/bfiltered/{fastq_file}-R2.clean.fastq.gz',
    output:
        fwd_clean_dedup = temp(OUT_DIR+'/{dataset}/preQC/cdedupe/{fastq_file}-R1.clean.nodup.fastq.gz'),
        rev_clean_dedup = temp(OUT_DIR+'/{dataset}/preQC/cdedupe/{fastq_file}-R2.clean.nodup.fastq.gz')
    log:
        OUT_DIR + '/{dataset}/preQC/logs/cdedupe/{fastq_file}.log'
    threads:cpus_avail
    params:
        dd_mem_gb = config['preprocess']['filter_human']['bbmap_mem']
    singularity: singularity_preqc
    group: 'preprocess'
    message: "Running dedupe with {threads} cores."
    shell:
        '''
        (clumpify.sh \
        in1={input.fwd_clean} in2={input.rev_clean} \
        out1={output.fwd_clean_dedup} \
        out2={output.rev_clean_dedup} \
        dedupe=t \
        t={threads} \
        -Xmx{params.dd_mem_gb}g -eoom) &>{log}
        '''
        #(dedupe.sh \
        #in1={input.fwd_clean} in2={input.rev_clean} \
        #out=stdout.fq \
        #outd={output.fastq_duplicates} \
        #ac=f minidentity=99 \
        #-Xmx{params.dd_mem_gb}g| \
        #reformat.sh \
        #int=t in=stdin.fq \
        #out1={output.fwd_clean_dedup} \
        #out2={output.rev_clean_dedup} \
        #threads={threads}) &>{log}
        #'''
# After removal of adapters, human reads, and duplicates,
# the reads' 3'end are quality trimmed (cut by quality score) with fastp
# Notice that adapter- and quality- filtering are disabled because it was done by rule trim_adapters
rule trim_3end:
    input:
        fwd_clean_dedup = OUT_DIR+'/{dataset}/preQC/cdedupe/{fastq_file}-R1.clean.nodup.fastq.gz',
        rev_clean_dedup = OUT_DIR+'/{dataset}/preQC/cdedupe/{fastq_file}-R2.clean.nodup.fastq.gz'
    params:
        min_length = config['preprocess']['trim_adapters']['min_length']
    output:
        fwd_tr = temp(OUT_DIR+'/{dataset}/preQC/dfinaltrim/{fastq_file}-R1.clean.nodup.fastp.fastq.gz'),
        rev_tr = temp(OUT_DIR+'/{dataset}/preQC/dfinaltrim/{fastq_file}-R2.clean.nodup.fastp.fastq.gz'),
        report1 = OUT_DIR+'/{dataset}/preQC/dfinaltrim/{fastq_file}.clean.nodup.fastp.html',
        report2 = OUT_DIR+'/{dataset}/preQC/dfinaltrim/{fastq_file}.clean.nodup.fastp.json'
    log:
        OUT_DIR + '/{dataset}/preQC/logs/dfinaltrim/{fastq_file}.log'
    threads: cpus_avail
    singularity: singularity_preqc
    group: 'preprocess'
    message: "Running trim3end_dedupe with {threads} cores ..."
    shell:
        '''
        (fastp \
        --disable_quality_filtering \
        --disable_adapter_trimming \
        --length_required {params.min_length} \
        --cut_tail -W 4 -M 20 \
        --in1 {input.fwd_clean_dedup} --in2 {input.rev_clean_dedup} \
        --out1 {output.fwd_tr} --out2 {output.rev_tr} \
        --html {output.report1} --json {output.report2} \
        --thread {threads}) &>{log}
        '''
###**THESE TARGET FILES ARE THE FINAL CLEAN**###
# concatenate quality/length filtered, adapter-trimmed, cleaned, deduplicated and
# quality-trimmed fastqs from the same samples
rule concatenate_fastqs:
    input:
        #
        sample_fwds = lambda wildcards: ['{}/{}/preQC/dfinaltrim/{}-{}_{}-R1.clean.nodup.fastp.fastq.gz'.format(
        OUT_DIR, DATASETS[ix], value, RUNS[ix], LANES[ix])
        for ix,value in enumerate(SAMPLES) if (value == wildcards.sample) and (DATASETS[ix] == wildcards.dataset)],
        #
        sample_revs = lambda wildcards: ['{}/{}/preQC/dfinaltrim/{}-{}_{}-R2.clean.nodup.fastp.fastq.gz'.format(
        OUT_DIR, DATASETS[ix], value, RUNS[ix], LANES[ix])
        for ix,value in enumerate(SAMPLES) if (value==wildcards.sample) and (DATASETS[ix]==wildcards.dataset)]
    output:
        sample_fwd = protected(OUT_DIR + '/{dataset}/preQC/emerged/{sample}-R1.fastq.gz'),
        sample_rev = protected(OUT_DIR + '/{dataset}/preQC/emerged/{sample}-R2.fastq.gz')
    wildcard_constraints:
        sample = '\w+'
    message: "Running concatenate_fastqs ..."
    shell:
        '''
        cat {input.sample_fwds} > {output.sample_fwd}
        cat {input.sample_revs} > {output.sample_rev}
        '''
# Check that for each sample, fastq files were concatenated
rule check_concatenation:
    input:
        inputs_r1 = list_concatenated_r1_fastqs,
        inputs_r2 = list_concatenated_r2_fastqs
    output:
        concatenated_fastqs_list = OUT_DIR + '/{dataset}/preQC/summary_stats/' +
                                    '{dataset}_concatenation_all.done'
    #conda:'../envs/rawQC.yaml'
    run:
        import os
        from pathlib import Path

        try:
            sample_fastqs_exist = [os.path.exists(r1_file) and os.path.exists(r2_file)
                                   for r1_file, r2_file in zip(input.inputs_r1,input.inputs_r2)]
            if all(sample_fastqs_exist):
                Path(output.concatenated_fastqs_list).touch()
        except (TypeError, ValueError) as e:
            print(e)

# MultiQC/multi-file reports
#----------------------------

# List files to include in MultiQC/multi-file reports

#
rule mqc_trim_adapters_list_files:
    input:
        #
        inputs_list = mqc_trim_adapters_inputs
    output:
        multiqc_inputs_file = OUT_DIR +'/{dataset}/preQC/multiqc/atrimmed/multiqc_inputs.txt'
    run:
        import os
        try:
            os.makedirs(os.path.dirname(output.multiqc_inputs_file))
        except OSError:
            pass

        with open(output.multiqc_inputs_file, mode='w', encoding='utf-8') as out:
            for item in input.inputs_list:
                out.write("%s\n" % item)
#
rule mqc_filter_human_list_files:
    input:
        inputs_list = mqc_filter_human_inputs
    output:
        multiqc_inputs_file = OUT_DIR + '/{dataset}/preQC/multiqc/bfiltered/multiqc_inputs.txt'
    run:
        import os
        try:
            os.makedirs(os.path.dirname(output.multiqc_inputs_file))
        except OSError:
            pass

        with open(output.multiqc_inputs_file, mode='w', encoding='utf-8') as out:
            for item in input.inputs_list:
                out.write("%s\n" % item)
#
rule mqc_trim_3end_list_files:
    input:
        inputs_list = mqc_trim_3end_inputs
    output:
        multiqc_inputs_file = OUT_DIR + '/{dataset}/preQC/multiqc/dfinaltrim/multiqc_inputs.txt'
    run:
        import os
        try:
            os.makedirs(os.path.dirname(output.multiqc_inputs_file))
        except OSError:
            pass

        with open(output.multiqc_inputs_file, mode='w', encoding='utf-8') as out:
            for item in input.inputs_list:
                out.write("%s\n" % item)

# Run MultiQC on all pre-processed reports
#
rule mqc_trim_adapters:
    input:
        OUT_DIR +'/{dataset}/preQC/multiqc/atrimmed/multiqc_inputs.txt'
    output:
        multiqc_data_dir = OUT_DIR +
                            '/{dataset}/preQC/multiqc/atrimmed'+
                            '/{dataset}_atrimmed_mqc_data/multiqc_data.json',
        multiqc_report = report(OUT_DIR +
                                '/{dataset}/preQC/multiqc/atrimmed' +
                                '/{dataset}_atrimmed_mqc.html',
                                category='preQC_step1:trim_adapters')
    singularity: singularity_preqc
    shell:
        '''
        multiqc -f --file-list {input} --filename {output.multiqc_report}
        '''
#
rule mqc_filter_human:
    input:
        OUT_DIR +'/{dataset}/preQC/multiqc/bfiltered/multiqc_inputs.txt'
    output:
        mqc_report = report(OUT_DIR +
                            '/{dataset}/preQC/multiqc/bfiltered' +
                            '/{dataset}_bfiltered_stats.tsv',
                            category='preQC_step2:filter_human')
    #conda:'../envs/rawQC.yaml'
    run:
        from utils import summarize_filter_human_step
        mqc_stats_data = summarize_filter_human_step('{}'.format(input))
        mqc_stats_data.to_csv(output.mqc_report, sep='\t', header=True,
                              index=True, index_label='Unit')
#
rule multiqc_trim_3end:
    input:
        OUT_DIR+'/{dataset}/preQC/multiqc/dfinaltrim/multiqc_inputs.txt'
    output:
        multiqc_fastqc = OUT_DIR +
                            '/{dataset}/preQC/multiqc/dfinaltrim'+
                            '/{dataset}_dfinaltrim_mqc_data/multiqc_data.json',
        multiqc_report = report(OUT_DIR +
                                '/{dataset}/preQC/multiqc/dfinaltrim'+
                                '/{dataset}_dfinaltrim_mqc.html',
                                category='preQC_step4:trim_3end')
    singularity: singularity_preqc
    shell:
        '''
        multiqc -f --file-list {input} --filename {output.multiqc_report}
        '''
#
# Create summarizing tables and plots
rule summarize_preQC:
    input:
        trim_adapters_mqc_json = OUT_DIR + '/{dataset}/preQC/multiqc/atrimmed'+
                                 '/{dataset}_atrimmed_mqc_data/multiqc_data.json',
        filter_human_mqc_stats = OUT_DIR + '/{dataset}/preQC/multiqc/bfiltered' +
                                 '/{dataset}_bfiltered_stats.tsv',
        trim3end_mqc_json = OUT_DIR + '/{dataset}/preQC/multiqc/dfinaltrim'+
                            '/{dataset}_dfinaltrim_mqc_data/multiqc_data.json'
    log:
        OUT_DIR + '/{dataset}/preQC/logs/summarize_preqc.log'
    output:
        units_summary = report(OUT_DIR +
                               '/{dataset}/preQC/summary_stats' +
                               '/{dataset}_preqc_units_summary.tsv',
                               category='preQC:summaries'),
        samples_summary = report(OUT_DIR +
                                 '/{dataset}/preQC/summary_stats' +
                                 '/{dataset}_preqc_samples_summary.tsv',
                                 category='preQC:summaries'),
        units_summary_plot = report(OUT_DIR +
                                    '/{dataset}/preQC/summary_stats' +
                                    '/{dataset}_preqc_units_barchart.svg',
                                    category='preQC:summaries'),
        samples_summary_plot = report(OUT_DIR +
                                      '/{dataset}/preQC/summary_stats' +
                                      '/{dataset}_preqc_samples_barchart.svg',
                                      category='preQC:summaries'),
        units_summary_pct_plot = report(OUT_DIR +
                                        '/{dataset}/preQC/summary_stats' +
                                        '/{dataset}_preqc_units_pct_barchart.svg',
                                        category='preQC:summaries'),
        samples_summary_pct_plot = report(OUT_DIR +
                                          '/{dataset}/preQC/summary_stats'+
                                          '/{dataset}_preqc_samples_pct_barchart.svg',
                                          category='preQC:summaries')
    #conda:'../envs/rawQC.yaml'
    run:
        from utils import summarize_preqc
        from utils import plot_preqc_summary
        import sys

        sys.stdout = open(log[0], 'w')


        try:
            trim_adapters_mqc_json = '{}'.format(input.trim_adapters_mqc_json)
            filter_human_mqc_stats = '{}'.format(input.filter_human_mqc_stats)
            trim3end_mqc_json = '{}'.format(input.trim3end_mqc_json)
            # units summary
            units_summary_df = summarize_preqc(trim_adapters_mqc_json, filter_human_mqc_stats,
                                               trim3end_mqc_json)
        except (TypeError, ValueError) as e:
            print(e)
        else:
            units_summary_df.to_csv(output.units_summary, sep='\t', header=True, index=True,
                                    index_label='Unit')
            # samples summary
            samples_summary_df = units_summary_df.groupby(['Sample']).agg('sum')
            pct_columns = ['trim_adapters_pct_disc', 'filter_human_pct_disc', 'dedupe_pct_disc',
                           'trim_3end_pct_disc', 'total_pct_disc', 'total_pct_kept']
            samples_summary_df[pct_columns] = units_summary_df[['Sample']+pct_columns].groupby(['Sample']).agg('mean')
            samples_summary_df.to_csv(output.samples_summary, sep='\t', header=True, index=True,
                                      index_label='Sample')
            # radial barcharts
            #
            fig1 = plot_preqc_summary(units_summary_df, by='units', plot_type='raw')
            fig1.savefig(output.units_summary_plot, bbox_inches='tight', format='svg')
            #
            fig2 = plot_preqc_summary(units_summary_df, by='units', plot_type='pct')
            fig2.savefig(output.units_summary_pct_plot, bbox_inches='tight', format='svg')
            #
            fig3 = plot_preqc_summary(units_summary_df, by='samples', plot_type='raw')
            fig3.savefig(output.samples_summary_plot, bbox_inches='tight', format='svg')
            #
            fig4 = plot_preqc_summary(units_summary_df, by='samples', plot_type='pct')
            fig4.savefig(output.samples_summary_pct_plot, bbox_inches='tight', format='svg')
        finally:
            sys.stdout.close()
