'''
Author: Monica R. Ticlla
Afiliation(s): SIB, SwissTPH, UNIBAS
Description: rules for functional profiling of paired-end shotgun DNA metagenomic
sequencing data with HUMAnN2.
'''

##----------------------------------------------------------------------------##
## Import modules
##----------------------------------------------------------------------------##


##----------------------------------------------------------------------------##
## Local rules
##----------------------------------------------------------------------------##
localrules:
    humann2_check

##----------------------------------------------------------------------------##
## Local variables
##----------------------------------------------------------------------------##
singularity_humann = METAPROF_SIF

##----------------------------------------------------------------------------##
## Helper functions
##----------------------------------------------------------------------------##
# def get_first_forward_fastq_in_dataset(wildcards):
#     FIRST_SAMPLE = DATASETS_SAMPLES_DICT[wildcards.dataset][0]
#     FASTQ_FWD = '{}/{}/preQC/emerged/{}-R1.fastq.gz'.format(OUT_DIR,
#                                                             wildcards.dataset,
#                                                             FIRST_SAMPLE)
#     return FASTQ_FWD
#
# def get_first_rev_fastq_in_dataset(wildcards):
#     FIRST_SAMPLE = DATASETS_SAMPLES_DICT[wildcards.dataset][0]
#     FASTQ_REV = '{}/{}/preQC/emerged/{}-R2.fastq.gz'.format(OUT_DIR,
#                                                             wildcards.dataset,
#                                                             FIRST_SAMPLE)
#     return FASTQ_REV

def get_all_samples_genefamilies(wildcards):
    SAMPLES_W_PROF = DATASETS_SAMPLES_DICT[wildcards.dataset]
    return expand('{out_dir}/{dataset}/Humann2/profiles/{sample}/{sample}_genefamilies.tsv',
                  out_dir=OUT_DIR, dataset=wildcards.dataset, sample=SAMPLES_W_PROF)
def get_all_samples_genefamilies_relab(wildcards):
    SAMPLES_W_PROF_RELAB = DATASETS_SAMPLES_DICT[wildcards.dataset]
    return expand('{out_dir}/{dataset}/Humann2/profiles_norm/{sample}/{sample}_genefamilies_relab.tsv',
                  out_dir=OUT_DIR, dataset=wildcards.dataset, sample=SAMPLES_W_PROF_RELAB)
def get_all_samples_genefamilies_cpm(wildcards):
    SAMPLES_W_PROF_CPM = DATASETS_SAMPLES_DICT[wildcards.dataset]
    return expand('{out_dir}/{dataset}/Humann2/profiles_norm/{sample}/{sample}_genefamilies_cpm.tsv',
                  out_dir=OUT_DIR, dataset=wildcards.dataset, sample=SAMPLES_W_PROF_CPM)

def get_all_samples_pathabundances(wildcards):
    SAMPLES_W_PATHABUND = DATASETS_SAMPLES_DICT[wildcards.dataset]
    return expand('{out_dir}/{dataset}/Humann2/profiles/{sample}/{sample}_pathabundance.tsv',
                  out_dir=OUT_DIR, dataset=wildcards.dataset, sample=SAMPLES_W_PATHABUND)
def get_all_samples_pathabundances_relab(wildcards):
    SAMPLES_W_PATHABUND_RELAB = DATASETS_SAMPLES_DICT[wildcards.dataset]
    return expand('{out_dir}/{dataset}/Humann2/profiles_norm/{sample}/{sample}_pathabundance_relab.tsv',
                  out_dir=OUT_DIR, dataset=wildcards.dataset, sample=SAMPLES_W_PATHABUND_RELAB)
def get_all_samples_pathabundances_cpm(wildcards):
    SAMPLES_W_PATHABUND_CPM = DATASETS_SAMPLES_DICT[wildcards.dataset]
    return expand('{out_dir}/{dataset}/Humann2/profiles_norm/{sample}/{sample}_pathabundance_cpm.tsv',
                  out_dir=OUT_DIR, dataset=wildcards.dataset, sample=SAMPLES_W_PATHABUND_CPM)

def get_all_samples_pathcoverage(wildcards):
    SAMPLES_W_PATHCOV = DATASETS_SAMPLES_DICT[wildcards.dataset]
    return expand('{out_dir}/{dataset}/Humann2/profiles/{sample}/{sample}_pathcoverage.tsv',
                  out_dir=OUT_DIR, dataset=wildcards.dataset, sample=SAMPLES_W_PATHCOV)


rule humann2_profiling:
    input:
        sample_fwd = OUT_DIR + '/{dataset}/preQC/emerged/{sample}-R1.fastq.gz',
        sample_rev = OUT_DIR + '/{dataset}/preQC/emerged/{sample}-R2.fastq.gz',
        metaphlan2_abund = OUT_DIR + '/{dataset}/PhlAnProf/metaphlan/profiles_abund/{sample}.txt'
        #choco_ixs = OUT_DIR + '/{dataset}/Humann2/choco_custom_ixs/choco_humann2_temp'
    output:
        genefamilies = OUT_DIR + '/{dataset}/Humann2/profiles/{sample}/{sample}_genefamilies.tsv',
        pathabundances = OUT_DIR + '/{dataset}/Humann2/profiles/{sample}/{sample}_pathabundance.tsv',
        pathcoverage = OUT_DIR + '/{dataset}/Humann2/profiles/{sample}/{sample}_pathcoverage.tsv',
        sample_log = OUT_DIR + '/{dataset}/Humann2/profiles/{sample}/{sample}.log'
    log:
        OUT_DIR + '/{dataset}/Humann2/logs/profiles/{sample}.log'
    params:
        fastq_tmp = OUT_DIR + '/{dataset}/Humann2/profiles/{sample}/{sample}.fastq.gz',
        prot_db = METASNK_DB_DIR+'/humann2_databases/uniref',
        nt_db = METASNK_DB_DIR+'/humann2_databases/chocophlan'
    threads:cpus_avail
    singularity:singularity_humann
    message: 'Running HUMAnN2 on sample {wildcards.sample}'
    group: 'HUMAnN2_prof_norm'
    shell:
        '''
        (bash -c "source activate humann2 && \
        cat {input.sample_fwd} {input.sample_rev} >{params.fastq_tmp}; \
        humann2 \
        --input {params.fastq_tmp} \
        --output $(dirname {output.genefamilies}) \
        --o-log {output.sample_log} \
        --taxonomic-profile {input.metaphlan2_abund} \
        --protein-database {params.prot_db} \
        --nucleotide-database {params.nt_db} \
        --threads {threads} \
        --remove-temp-output \
        --memory-use maximum; \
        rm {params.fastq_tmp}") &>{log}
        '''
rule humann2_normalize:
    input:
        genefamilies = OUT_DIR + '/{dataset}/Humann2/profiles/{sample}/{sample}_genefamilies.tsv',
        pathabundances = OUT_DIR + '/{dataset}/Humann2/profiles/{sample}/{sample}_pathabundance.tsv'
    output:
        genefamilies_relab = OUT_DIR + '/{dataset}/Humann2/profiles_norm/{sample}/{sample}_genefamilies_relab.tsv',
        genefamilies_cpm = OUT_DIR + '/{dataset}/Humann2/profiles_norm/{sample}/{sample}_genefamilies_cpm.tsv',
        pathabundances_relab = OUT_DIR + '/{dataset}/Humann2/profiles_norm/{sample}/{sample}_pathabundance_relab.tsv',
        pathabundances_cpm = OUT_DIR + '/{dataset}/Humann2/profiles_norm/{sample}/{sample}_pathabundance_cpm.tsv',
    log:
        OUT_DIR + '/{dataset}/Humann2/logs/profiles_norm/{sample}.log'
    singularity:singularity_humann
    message: 'Normalizing HUMAnN2 outputs for sample {wildcards.sample}'
    group: 'HUMAnN2_prof_norm'
    shell:
        '''
        (bash -c "source activate humann2 && \
        humann2_renorm_table \
        --input {input.genefamilies} \
        --output {output.genefamilies_relab} \
        --units relab \
        --update-snames; \
        humann2_renorm_table \
        --input {input.genefamilies} \
        --output {output.genefamilies_cpm} \
        --units cpm \
        --update-snames; \
        humann2_renorm_table \
        --input {input.pathabundances} \
        --output {output.pathabundances_relab} \
        --units relab \
        --update-snames; \
        humann2_renorm_table \
        --input {input.pathabundances} \
        --output {output.pathabundances_cpm} \
        --units cpm \
        --update-snames") &>{log}
        '''
rule humann2_merging:
    input:
        genefamilies = get_all_samples_genefamilies,
        genefamilies_relab = get_all_samples_genefamilies_relab,
        genefamilies_cpm = get_all_samples_genefamilies_cpm,
        pathabundances = get_all_samples_pathabundances,
        pathabundances_relab = get_all_samples_pathabundances_relab,
        pathabundances_cpm = get_all_samples_pathabundances_cpm,
        pathcoverage = get_all_samples_pathcoverage
    output:
        genefamilies_merged = report(OUT_DIR + '/{dataset}/Humann2/profiles_merged/{dataset}_genefamilies.tsv',
                                     category='HUMAnN2Prof'),
        genefamilies_relab_merged = report(OUT_DIR + '/{dataset}/Humann2/profiles_merged/{dataset}_genefamilies_relab.tsv',
                                           category='HUMAnN2Prof'),
        genefamilies_cpm_merged = report(OUT_DIR + '/{dataset}/Humann2/profiles_merged/{dataset}_genefamilies_cpm.tsv',
                                         category='HUMAnN2Prof'),
        pathabund_merged = report(OUT_DIR + '/{dataset}/Humann2/profiles_merged/{dataset}_pathabundance.tsv',
                                  category='HUMAnN2Prof'),
        pathabund_relab_merged = report(OUT_DIR + '/{dataset}/Humann2/profiles_merged/{dataset}_pathabundance_relab.tsv',
                                        category='HUMAnN2Prof'),
        pathabund_cpm_merged = report(OUT_DIR + '/{dataset}/Humann2/profiles_merged/{dataset}_pathabundance_cpm.tsv',
                                      category='HUMAnN2Prof'),
        pathcov_merged = report(OUT_DIR + '/{dataset}/Humann2/profiles_merged/{dataset}_pathcoverage.tsv',
                                category='HUMAnN2Prof')
    log:
        OUT_DIR + '/{dataset}/Humann2/logs/humann2_merging.log'
    singularity:singularity_humann
    message: 'Joining HUMAnN2 outputs for dataset {wildcards.dataset}'
    shell:
        '''
        (bash -c "source activate humann2 && \
        humann2_join_tables \
        --input $(dirname $(dirname {input.genefamilies[0]})) \
        --output {output.genefamilies_merged} \
        --file_name genefamilies \
        --search-subdirectories; \
        humann2_join_tables \
        --input $(dirname $(dirname {input.genefamilies_relab[0]})) \
        --output {output.genefamilies_relab_merged} \
        --file_name genefamilies_relab \
        --search-subdirectories; \
        humann2_join_tables \
        --input $(dirname $(dirname {input.genefamilies_cpm[0]})) \
        --output {output.genefamilies_cpm_merged} \
        --file_name genefamilies_cpm \
        --search-subdirectories; \
        humann2_join_tables \
        --input $(dirname $(dirname {input.pathabundances[0]})) \
        --output {output.pathabund_merged} \
        --file_name pathabundance \
        --search-subdirectories; \
        humann2_join_tables \
        --input $(dirname $(dirname {input.pathabundances_relab[0]})) \
        --output {output.pathabund_relab_merged} \
        --file_name pathabundance_relab \
        --search-subdirectories; \
        humann2_join_tables \
        --input $(dirname $(dirname {input.pathabundances_cpm[0]})) \
        --output {output.pathabund_cpm_merged} \
        --file_name pathabundance_cpm \
        --search-subdirectories; \
        humann2_join_tables \
        --input $(dirname $(dirname {input.pathcoverage[0]})) \
        --output {output.pathcov_merged} \
        --file_name pathcoverage \
        --search-subdirectories") &>{log}
        '''
rule humann2_check:
    input:
        genefamilies_merged = OUT_DIR + '/{dataset}/Humann2/profiles_merged/{dataset}_genefamilies.tsv',
        genefamilies_relab_merged = OUT_DIR + '/{dataset}/Humann2/profiles_merged/{dataset}_genefamilies_relab.tsv',
        genefamilies_cpm_merged = OUT_DIR + '/{dataset}/Humann2/profiles_merged/{dataset}_genefamilies_cpm.tsv',
        pathabund_merged = OUT_DIR + '/{dataset}/Humann2/profiles_merged/{dataset}_pathabundance.tsv',
        pathabund_relab_merged = OUT_DIR + '/{dataset}/Humann2/profiles_merged/{dataset}_pathabundance_relab.tsv',
        pathabund_cpm_merged = OUT_DIR + '/{dataset}/Humann2/profiles_merged/{dataset}_pathabundance_cpm.tsv',
        pathcov_merged = OUT_DIR + '/{dataset}/Humann2/profiles_merged/{dataset}_pathcoverage.tsv'
    output:
        check = OUT_DIR + '/{dataset}/Humann2/all.done'
    shell:
        '''
        touch {output.check}
        '''
#rule humann2_renaming:
