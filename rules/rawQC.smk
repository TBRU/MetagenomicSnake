'''
Author: Monica R. Ticlla
Afiliation(s): SIB, SwissTPH, UNIBAS
Description: rules for QC and pre-processing of paired-end shotgun DNA
metagenomic sequencing.
'''

localrules:
    multiqc_raw_list_files,
    multiqc_raw

##---------------------------------------------------------------------------##
## Local variables
##----------------------------------------------------------------------------##
#singularity_img = 'shub://mticlla/MetagenomicSnake:preqc_v0_1'
singularity_img = PREQC_SIF
##----------------------------------------------------------------------------##
## Rules with target files
##----------------------------------------------------------------------------##
# This rule only processess a subset of reads (10%) per fastq file, and only
# for R1. 'R1' was removed from names in html and zip outputs
rule fastqc_raw:
    input:
        lambda wildcards: ['{}/{}/fastq/{}-R1.fastq.gz'.format(RAW_FASTQ_DIR, DATASETSX[ix], value)
        for ix,value in enumerate(FASTQS) if value==wildcards.fastq_file]
    params:
        fastqc_dir = OUT_DIR,
        samplerate = config['rawQC']['samplerate']
    log:
        OUT_DIR+'/{dataset}/rawQC/logs/{fastq_file}_fastqc.log'
    output:
        fastqc_html = OUT_DIR+'/{dataset}/rawQC/fastqc/{fastq_file}_fastqc.html',
        fastqc_zip = OUT_DIR+'/{dataset}/rawQC/fastqc/{fastq_file}_fastqc.zip'
    threads: cpus_avail
    singularity: singularity_img
    message:
        "Executing FatQC on {input} with {threads} threads and using {params.samplerate} times total reads."
    shell:
        '''
        # Take a random sample of reads and process them with fastQC
        (reformat.sh in={input} out=stdout.fq samplerate={params.samplerate} | \
        fastqc -o {params.fastqc_dir}/{wildcards.dataset}/rawQC/fastqc -f fastq \
        -t {threads} stdin:{wildcards.fastq_file}) 2> {log}
        '''

# List files for MultiQC
rule multiqc_raw_list_files:
    input:
        #all fastqc zip files per dataset
        fastqcs=lambda wildcards: ['{}/{}/rawQC/fastqc/{}_fastqc.zip'.format(OUT_DIR, value, FASTQS[ix])
        for ix,value in enumerate(DATASETSX) if value==wildcards.dataset]
    output:
        #multiqc_input_list = RAW_QC_REPORT+'/{dataset}/multiqc_inputs.txt'
        multiqc_input_list = OUT_DIR+'/{dataset}/rawQC/multiqc_inputs.txt'
    run:
        import os
        try:
            os.makedirs(os.path.dirname(output.multiqc_input_list))
        except OSError:
            pass

        with open(output.multiqc_input_list, mode='w', encoding='utf-8') as out:
            for item in input.fastqcs:
                out.write("%s\n" % item)

# Run MultiQC on all FastQC reports
rule multiqc_raw:
    input:
        multiqc_input_list = OUT_DIR+'/{dataset}/rawQC/multiqc_inputs.txt'
    params:
        multiqc_dir = OUT_DIR+'/{dataset}/rawQC/multiqc'
    output:
        multiqc_fastqc = OUT_DIR + '/{dataset}/rawQC/multiqc/{dataset}_multiqc_data/multiqc_fastqc.txt',
        multiqc_report = report(OUT_DIR + '/{dataset}/rawQC/multiqc/{dataset}_multiqc.html', 
                         category='RawQC', 
                         caption='../report/rawQC/rawQC_multiqc_fastqc.rst')
    singularity:singularity_img
    shell:
        '''
        multiqc -f --file-list {input} --filename {output.multiqc_report}
        '''
# Creates summarizing tables and plots
rule summarize_raw:
    input:
        OUT_DIR + '/{dataset}/rawQC/multiqc/{dataset}_multiqc_data/multiqc_fastqc.txt'
    log:
        OUT_DIR+'/{dataset}/rawQC/logs/summarize_raw.log'
    output:
        units_stats = report(OUT_DIR + '/{dataset}/rawQC/summary_stats/{dataset}_units_stats.txt', 
                      category='RawQC', 
                      caption='../report/rawQC/rawQC_units_stats.rst'),
        samples_stats = report(OUT_DIR + '/{dataset}/rawQC/summary_stats/{dataset}_samples_stats.txt', 
                        category='RawQC', 
                        caption='../report/rawQC/rawQC_samples_stats.rst'),
        run_lane_stats = report(OUT_DIR + '/{dataset}/rawQC/summary_stats/{dataset}_run_lane_stats.txt', 
                         category='RawQC', 
                         caption='../report/rawQC/rawQC_run_lane_stats.rst'),
        units_read_pairs_plot = report(OUT_DIR + '/{dataset}/rawQC/summary_stats/{dataset}_units_read_dist.svg', 
                                category='RawQC', 
                                caption='../report/rawQC/rawQC_units_read_dist.rst'),
        samples_read_pairs_plot = report(OUT_DIR + '/{dataset}/rawQC/summary_stats/{dataset}_samples_read_dist.svg', 
                                  category='RawQC', 
                                  caption='../report/rawQC/rawQC_samples_read_dist.rst'),
        samples_read_pairs_dispersion_plot = report(OUT_DIR + '/{dataset}/rawQC/summary_stats/{dataset}_samples_read_dispersion.svg', 
                                             category='RawQC', 
                                             caption='../report/rawQC/rawQC_samples_reads_dispersion.rst'),
        fastqc_tests_status_plot = report(OUT_DIR + '/{dataset}/rawQC/summary_stats/{dataset}_fastqc_tests_status.svg', 
                                   category='RawQC', 
                                   caption='../report/rawQC/rawQC_fastqc_tests_status.rst')
    conda:'../envs/rawQC.yaml'
    script:
        '../scripts/rawQC_summarize.py'
