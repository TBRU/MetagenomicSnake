'''
Author: Monica R. Ticlla
Afiliation(s): SIB, SwissTPH, UNIBAS
Description: rules for pre-processing of paired-end shotgun DNA metagenomic
sequencing.
'''

##----------------------------------------------------------------------------##
## Import modules
##----------------------------------------------------------------------------##


##----------------------------------------------------------------------------##
## Local rules
##----------------------------------------------------------------------------##
localrules:
    metaphlan2_merge,
    metaphlan2_visualize,
    strphlan_summary
##----------------------------------------------------------------------------##
## Local variables
##----------------------------------------------------------------------------##
singularity_metaprof = METAPROF_SIF

##----------------------------------------------------------------------------##
## Helper functions
##----------------------------------------------------------------------------##
def get_all_samples_profiles(wildcards):
    SAMPLES_W_PROF = DATASETS_SAMPLES_DICT[wildcards.dataset]
    return expand('{out_dir}/{dataset}/PhlAnProf/metaphlan/profiles_abund/{sample}.txt',
                  out_dir=OUT_DIR, dataset=wildcards.dataset, sample=SAMPLES_W_PROF)

def get_all_samples_prof_counts(wildcards):
    SAMPLES_W_PROF_COUNTS = DATASETS_SAMPLES_DICT[wildcards.dataset]
    return expand('{out_dir}/{dataset}/PhlAnProf/metaphlan/profiles_counts/{sample}.txt',
                  out_dir=OUT_DIR, dataset=wildcards.dataset, sample=SAMPLES_W_PROF_COUNTS)

def get_all_samples_markers(wildcards):
    SAMPLES_W_MARKERS = DATASETS_SAMPLES_DICT[wildcards.dataset]
    return expand('{out_dir}/{dataset}/PhlAnProf/strphlan/markers/{sample}.markers',
                  out_dir=OUT_DIR, dataset=wildcards.dataset, sample=SAMPLES_W_MARKERS)

##----------------------------------------------------------------------------##
## Rules with target files
##----------------------------------------------------------------------------##
rule metaphlan2_profile:
    input:
        sample_fwd = OUT_DIR + '/{dataset}/preQC/emerged/{sample}-R1.fastq.gz',
        sample_rev = OUT_DIR + '/{dataset}/preQC/emerged/{sample}-R2.fastq.gz'
    output:
        profile_w_stats = OUT_DIR + '/{dataset}/PhlAnProf/metaphlan/profiles_stats/{sample}.txt',
        profile_counts = OUT_DIR + '/{dataset}/PhlAnProf/metaphlan/profiles_counts/{sample}.txt',
        profile_abund = OUT_DIR + '/{dataset}/PhlAnProf/metaphlan/profiles_abund/{sample}.txt',
        bowtie2out = OUT_DIR + '/{dataset}/PhlAnProf/metaphlan/bowtie2out/{sample}.bowtie2out.bz2',
        samout = OUT_DIR + '/{dataset}/PhlAnProf/metaphlan/samout/{sample}.sam.bz2'
    log:
        OUT_DIR + '/{dataset}/PhlAnProf/logs/metaphlan/{sample}.log'
    params:
        mpa_db = METASNK_DB_DIR + '/metaphlan_databases',
        proc = cpus_avail
    wildcard_constraints:
        sample = '\w+'
    singularity:singularity_metaprof
    shell:
        '''
        (metaphlan2.py {input.sample_fwd},{input.sample_rev} \
        --input_type fastq \
        --bowtie2db {params.mpa_db} \
        --index v20_m200 \
        -t rel_ab_w_read_stats \
        --bowtie2out {output.bowtie2out} \
        --samout {output.samout} \
        --output_file {output.profile_w_stats} \
        --nproc {params.proc};
        cut -f 1,5  {output.profile_w_stats} >{output.profile_counts}; \
        cut -f 1,2  {output.profile_w_stats} >{output.profile_abund}) &>{log}
        '''
rule metaphlan2_merge:
    input:
        profiles_w_stats = get_all_samples_profiles,
        profiles_counts = get_all_samples_prof_counts
    output:
        merged_abund = report(OUT_DIR + '/{dataset}/PhlAnProf/metaphlan' +
                              '/profiles_merged/{dataset}_abundances_table.txt',
                              category='PhlAnProf:Metaphlan2',
                              caption='../report/PhlAnProf/metaphlan2_abund.rst'),
        merged_counts = report(OUT_DIR + '/{dataset}/PhlAnProf/metaphlan' +
                               '/profiles_merged/{dataset}_counts_table.txt',
                               category='PhlAnProf:Metaphlan2',
                               caption='../report/PhlAnProf/metaphlan2_counts.rst'),
        merged_abund_sp = report(OUT_DIR + '/{dataset}/PhlAnProf/metaphlan' +
                                 '/profiles_merged/{dataset}_abundances_sp_table.txt',
                                 category='PhlAnProf:Metaphlan2',
                                 caption='../report/PhlAnProf/metaphlan2_abund_sp.rst'),
        merged_counts_sp = report(OUT_DIR + '/{dataset}/PhlAnProf/metaphlan' +
                                  '/profiles_merged/{dataset}_counts_sp_table.txt',
                                  category='PhlAnProf:Metaphlan2',
                                  caption='../report/PhlAnProf/metaphlan2_counts_sp.rst'),
        merged_abund_sp_clean = report(OUT_DIR + '/{dataset}/PhlAnProf/metaphlan' +
                                       '/profiles_merged/{dataset}_cleaned_abundances_sp_table.txt',
                                       category='PhlAnProf:Metaphlan2',
                                       caption='../report/PhlAnProf/metaphlan2_abund_sp_cleaned.rst'),
        merged_counts_sp_clean = report(OUT_DIR + '/{dataset}/PhlAnProf/metaphlan' +
                                        '/profiles_merged/{dataset}_cleaned_counts_sp_table.txt',
                                        category='PhlAnProf:Metaphlan2',
                                        caption='../report/PhlAnProf/metaphlan2_counts_sp_cleaned.rst'),
        samples_removed = report(OUT_DIR + '/{dataset}/PhlAnProf/metaphlan' +
                                 '/profiles_merged/{dataset}_samples_removed.txt',
                                 category='PhlAnProf:Metaphlan2',
                                 caption='../report/PhlAnProf/metaphlan2_samples_removed.rst')

    log:
        OUT_DIR + '/{dataset}/PhlAnProf/logs/metaphlan/merged.log'
    singularity:singularity_metaprof
    group:'metaphlan2_merging'
    shell:
        '''
        (# Merge samples' abundance profiles
        # ----------------------------------
        merge_metaphlan_tables.py {input.profiles_w_stats} > {output.merged_abund};
        merge_metaphlan_tables.py {input.profiles_counts} > {output.merged_counts};
        grep -E "(s__)|(^ID)" {output.merged_abund} | grep -v "t__" | sed 's/^.*s__//g' > {output.merged_abund_sp};
        grep -E "(s__)|(^ID)" {output.merged_counts} | grep -v "t__" | sed 's/^.*s__//g' > {output.merged_counts_sp};
        # Get column ixs of samples with only zero values in merged species-abundances table
        # ----------------------------------------------------------------------------------
        samples_to_keep_ix=$(cut -f 2- {output.merged_abund_sp} | tail -n +2 | awk '{{for (i=1;i<=NF;i++) sum[i]+=$i;}}; \
        END{{for (i in sum) if (sum[i]>0) print i+1;}}');
        # Remove samples with only zero values from species-abundances table
        # ------------------------------------------------------------------
        cut -f 1,$(echo ${{samples_to_keep_ix}} | tr " " ",") {output.merged_abund_sp} > {output.merged_abund_sp_clean};
        # Get list of samples removed
        # ---------------------------
        comm -13 <(head -n1 {output.merged_abund_sp_clean} | tr "\t" "\n"| sort) \
        <(head -n1 {output.merged_abund_sp} | tr "\t" "\n" | sort) > {output.samples_removed};
        # Get column ixs of samples with only zero values in merged species-counts table
        # ------------------------------------------------------------------------------
        samples_ixs=$(cut -f 2- {output.merged_counts_sp} | tail -n +2 | awk '{{for (i=1;i<=NF;i++) sum[i]+=$i;}}; \
        END{{for (i in sum) if (sum[i]>0) print i+1;}}');
        # Remove samples with only zero values from species-counts table
        # ---------------------------------------------------------------
        cut -f 1,$(echo ${{samples_ixs}} | tr " " ",") {output.merged_counts_sp} > {output.merged_counts_sp_clean};
        ) &>{log}
        '''
rule metaphlan2_visualize:
    input:
        merged_abund_sp = OUT_DIR + '/{dataset}/PhlAnProf/metaphlan/profiles_merged/{dataset}_cleaned_abundances_sp_table.txt'
    output:
        merged_abund_sp_png = report(OUT_DIR + '/{dataset}/PhlAnProf/metaphlan/profiles_merged/{dataset}_abundances_sp_heatmap.png',
                                     category='PhlAnProf:Metaphlan2',
                                     caption='../report/PhlAnProf/metaphlan2_abund_sp_heatmap.rst')
    log:
        OUT_DIR + '/{dataset}/PhlAnProf/logs/metaphlan/merged_abundances_sp_heatmap.log'
    singularity:singularity_metaprof
    group:'metaphlan2_merging'
    shell:
        '''
        (if [[ "$(head -n1 {input.merged_abund_sp} | awk '{{print NF}}')" -gt 2 ]];
            then
                hclust2.py \
                -i {input.merged_abund_sp} \
                -o {output.merged_abund_sp_png} \
                --f_dist_f braycurtis \
                --s_dist_f braycurtis \
                --cell_aspect_ratio 1 -l \
                --flabel_size 6 \
                --slabel_size 6 \
                --max_flabel_len 100 \
                --max_slabel_len 100 \
                --minv 0.1 \
                --dpi 300 \
                --image_size 50 \
                -c 'bbcyr'
            else
                echo 'hclust2.py expects at least two samples! It will generate and empty file!'; \
                touch {output.merged_abund_sp_png}
        fi) &>{log}
        '''
# Get the consensus of unique marker genes for each species found in the sample
rule strphlan_get_sample_markers:
    input:
        sample_sam = OUT_DIR + '/{dataset}/PhlAnProf/metaphlan/samout/{sample}.sam.bz2'
    output:
        sample_markers = OUT_DIR + '/{dataset}/PhlAnProf/strphlan/markers/{sample}.markers'
    log:
        OUT_DIR + '/{dataset}/PhlAnProf/logs/strphlan/markers/{sample}.log'
    singularity:singularity_metaprof
    threads:cpus_avail
    shell:
        '''
        (bash -c "source activate py27strphlan && sample2markers.py \
        --ifn_samples {input.sample_sam} \
        --input_type sam \
        --output_dir $(dirname {output.sample_markers}) \
        --nprocs {threads}") &>{log}
        '''
# Preliminar detection of clades detected in all samples, suitable for strain tracking analysis
# - first, run strainphlan to detect clades
# - keep only clades also detected by metaphlan, and
# - keep only those above user's defined minimal abundance threshold
checkpoint strphlan_clades_detection:
    input:
        sample_markers = get_all_samples_markers,
        merged_abundances_sp = OUT_DIR + '/{dataset}/PhlAnProf/metaphlan' +
                               '/profiles_merged/{dataset}_cleaned_abundances_sp_table.txt'
    output:
        clades_list = report(OUT_DIR + '/{dataset}/PhlAnProf/strphlan/{dataset}_clades.txt',
                             category='PhlAnProf:StrainPhlAn',
                             caption='../report/PhlAnProf/strphlan_clades_selected.rst'),
        clades_detected_dir = directory("{}/{{dataset}}/PhlAnProf/strphlan/clades_detected".format(OUT_DIR))
    log:
        OUT_DIR + '/{dataset}/PhlAnProf/logs/strphlan/clade_detection.log'
    params:
        mpa_pkl = METASNK_DB_DIR + '/metaphlan_databases/mpa_v20_m200.pkl'
    threads:cpus_avail
    singularity:singularity_metaprof
    shell:
        '''
        (bash -c 'if [[ "$(head -n1 {input.merged_abundances_sp} | awk "{{print NF}}")" -gt 2 ]]; then \
        source activate py27strphlan && strainphlan.py \
        --ifn_samples {input.sample_markers} \
        --mpa_pkl {params.mpa_pkl} \
        --output_dir $(dirname {output.clades_list}) \
        --nprocs_main {threads} \
        --print_clades_only | sort | \
        comm -12 - <(echo $(tail -n +2 {input.merged_abundances_sp} |  cut -f1 | sed "s/^/s__/g") | tr " " "\n"| sort) \
        >{output.clades_list}; \
        mkdir {output.clades_detected_dir}; \
        for clade in $(cat {output.clades_list}) ; do \
        touch {output.clades_detected_dir}/${{clade}}.clade; done; \
        else \
        echo "strainphlan.py expects at least two samples! It will generate and empty file!"; \
        > {output.clades_list};
        mkdir -p {output.clades_detected_dir}; \
        fi; echo $? ') &>{log}
        '''
rule strphlan_clades_markers_extraction:
    input:
        clade_name = OUT_DIR + '/{dataset}/PhlAnProf/strphlan/clades_detected/{clade}.clade',
        clade_dir = OUT_DIR + '/{dataset}/PhlAnProf/strphlan/clades_detected'
    output:
        clade_ref = touch(OUT_DIR + '/{dataset}/PhlAnProf/strphlan/clades_extracted/{clade}.done')
    log:
        OUT_DIR + '/{dataset}/PhlAnProf/logs/strphlan/clades_extracted/{clade}.log'
    params:
        mpa_pkl = METASNK_DB_DIR + '/metaphlan_databases/mpa_v20_m200.pkl',
        strpa_dir = METASNK_DB_DIR+'/strainphlan_markers',
        all_markers = METASNK_DB_DIR+'/strainphlan_markers/all_markers.fasta'
    singularity:singularity_metaprof
    group: 'strphlan_clade_analysis'
    shell:
        '''
        (bash -c 'if [ -f {params.strpa_dir}/{wildcards.clade}.markers.fasta ]; then \
        echo "{wildcards.clade} already extracted!"; \
        else \
        source activate py27strphlan && extract_markers.py \
        --mpa_pkl {params.mpa_pkl} \
        --ifn_markers {params.all_markers} \
        --clade {wildcards.clade} \
        --ofn_markers {params.strpa_dir}/{wildcards.clade}.markers.fasta ; \
        fi') &>{log}
        '''

rule strphlan_clade_profiling:
    input:
        samples_markers = get_all_samples_markers,
        clade_ref_markers = OUT_DIR + '/{dataset}/PhlAnProf/strphlan/clades_extracted/{clade}.done',
        samples_metadata = RAW_FASTQ_DIR + '/{dataset}/sample_metadata.tsv'
    output:
        clade_result_dir = directory('{}/{{dataset}}/PhlAnProf/strphlan/clades_profiled/{{clade}}'.format(OUT_DIR))
    log:
        clade_results = OUT_DIR + '/{dataset}/PhlAnProf/logs/strphlan/clades_profiled/{clade}.log'
    params:
        mpa_pkl = METASNK_DB_DIR + '/metaphlan_databases/mpa_v20_m200.pkl',
        strpa_dir = METASNK_DB_DIR+'/strainphlan_markers',
        metadatas = config['phlanprof']['strphlan_clade_profiling']['metadatas'],
        colorized_metadata = config['phlanprof']['strphlan_clade_profiling']['colorized_metadata']
    threads: cpus_avail
    singularity:singularity_metaprof
    group: 'strphlan_clade_analysis'
    shell:
        '''
        (bash -c 'source activate py27strphlan && mkdir -p {output.clade_result_dir}; \
        strainphlan.py \
        --ifn_samples {input.samples_markers} \
        --mpa_pkl {params.mpa_pkl} \
        --ifn_markers {params.strpa_dir}/{wildcards.clade}.markers.fasta \
        --clades {wildcards.clade} \
        --output_dir {output.clade_result_dir} \
        --nprocs_main {threads} \
        --relaxed_parameters; \
        if [ -f {output.clade_result_dir}/RAxML_bestTree.{wildcards.clade}.tree ]; then \
        add_metadata_tree.py \
        --ifn_trees {output.clade_result_dir}/RAxML_bestTree.{wildcards.clade}.tree \
        --ifn_metadatas {input.samples_metadata} \
        --metadatas {params.metadatas}; \
        plot_tree_graphlan.py \
        --ifn_tree {output.clade_result_dir}/RAxML_bestTree.{wildcards.clade}.tree.metadata \
        --colorized_metadata {params.colorized_metadata} \
        --leaf_marker_size 60 \
        --legend_marker_size 60; \
        deactivate; \
        source activate py27breadcrumbs; \
        strainphlan_ggtree.R \
        {output.clade_result_dir}/RAxML_bestTree.{wildcards.clade}.tree \
        {input.samples_metadata} \
        {output.clade_result_dir}/{wildcards.clade}.fasta \
        {output.clade_result_dir}/RAxML_bestTree.{wildcards.clade}.tree_1.png \
        {output.clade_result_dir}/RAxML_bestTree.{wildcards.clade}.tree_2.png || \
        echo "strainphlan_ggtree.R halted with non-zero exit status!"; fi') &>{log}
        '''

def aggregate_clade_profiles(wildcards):
    '''
    aggregate the file names of all the files
    generated at the strphlan2_clades_detection step
    '''
    checkpoint_output = checkpoints.strphlan_clades_detection.get(**wildcards).output[1]
    return expand('{out_dir}/{dataset}/PhlAnProf/strphlan/clades_profiled/{clade}',
                  out_dir=OUT_DIR,
                  dataset=wildcards.dataset,
                  clade=glob_wildcards(os.path.join(checkpoint_output, '{clade}.clade')).clade)

rule strphlan_summary:
    input:
        aggregate_clade_profiles
    output:
        #combined = OUT_DIR + '/{dataset}/PhlAnProf/strphlan/all.done',
        clades_profiled = report(OUT_DIR + '/{dataset}/PhlAnProf/strphlan/{dataset}_clades_profiled.tsv',
                                 category='PhlAnProf:StrainPhlAn',
                                 caption='../report/PhlAnProf/strphlan_clades_profiled.rst')
    log:
        OUT_DIR + '/{dataset}/PhlAnProf/logs/strphlan/strphlan_summary.log'
    run:
        import os
        from pathlib import Path
        import pandas as pd
        import collections
        import sys

        sys.stdout = open(log[0], 'w')

        clades_info_list = list()
        try:
            for clade_dir in input:
                clade_name = str(clade_dir).split('/')[-1]
                clade_info_file = str(clade_dir)+'/{}.info'.format(clade_name)
                if os.path.exists(clade_info_file):
                    with open(clade_info_file, mode='r') as f:
                        clade_info_dict = collections.OrderedDict((line.strip().split(':')[0],
                                                                   line.strip().split(':')[1].strip())
                                                                  for line in f)
                    clade_raxml_tree_png = '{}/RAxML_bestTree.{}.tree.metadata.png'.format(str(clade_dir),
                                                                                           clade_name)
                    if os.path.exists(clade_raxml_tree_png):
                        clade_info_dict['raxml_tree_png'] = clade_raxml_tree_png
                        clade_raxml_ggtree_1 = '{}/RAxML_bestTree.{}.tree_1.png'.format(str(clade_dir),
                                                                                        clade_name)
                        if os.path.exists(clade_raxml_ggtree_1):
                            clade_info_dict['raxml_ggtree_1_png'] = clade_raxml_ggtree_1
                            clade_raxml_ggtree_2 = '{}/RAxML_bestTree.{}.tree_2.png'.format(str(clade_dir),
                                                                                            clade_name)
                            if os.path.exists(clade_raxml_ggtree_2):
                                clade_info_dict['raxml_ggtree_2_png'] = clade_raxml_ggtree_2
                    clades_info_list.append(clade_info_dict)
        except (TypeError, ValueError) as e:
            print(e)
        else:
            clades_info_table = pd.DataFrame(clades_info_list)
            clades_info_table.to_csv(output.clades_profiled, sep='\t', header=True, index=False)
        finally:
            sys.stdout.close()
        #'''
        #touch {output.combined}
        #'''
