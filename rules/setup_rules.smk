# The main entry point of your workflow.
# After configuring, running snakemake -n in a clone of this repository should
# successfully execute a dry-run of the workflow.

##----------------------------------------------------------------------------##
## Local variables
##----------------------------------------------------------------------------##
# Variables already defined in the Snakefile
#METASNK_DB_DIR = os.environ["METASNK_DBS"]
#SHUB_PREQC_SIF = 'shub://mticlla/MetagenomicSnake:preqc_v0_1'
#SHUB_METAPROF_SIF = 'shub://mticlla/OmicSingularities:metaprof'
#PREQC_SIF = METASNK_DB_DIR+'/singularity/MetagenomicSnake_preqc_v0_1.sif'
#METAPROF_SIF = METASNK_DB_DIR+'/singularity/OmicSingularities_metaprof.sif'

##----------------------------------------------------------------------------##
## Local rules
##----------------------------------------------------------------------------##
localrules:
    pull_preqc_sif,
    pull_metaprof_sif,
    get_metaphlan_db,
    get_strphlan_db,
    get_panphlan_db,
    get_humann2_chocophlan,
    get_humann2_uniref,
    get_humann2_utility,
    get_humann2_dbs,
    get_human_ref

##----------------------------------------------------------------------------##
## Rules to download/build container/databases used by MetaSnk
##----------------------------------------------------------------------------##
# These rules require internet access
# Download singularity containers
rule pull_preqc_sif:
    output:
        # Singularity image for rawQC and preQC (about 800M)
        local_sif = PREQC_SIF
    params:
        shub_sif = SHUB_PREQC_SIF
    shell:
        '''
        [ ! -d $(dirname {output}) ] && mkdir $(dirname {output});
        singularity pull \
        --dir $(dirname {output.local_sif}) \
        $(basename {output}) \
        {params.shub_sif}
        '''
rule pull_metaprof_sif:
    output:
        # Singularity image for PhlAnProf (about 3G)
        local_sif = METAPROF_SIF
    params:
        shub_sif = SHUB_METAPROF_SIF
    shell:
        '''
        [ ! -d $(dirname {output}) ] && mkdir $(dirname {output});
        singularity pull \
        --dir $(dirname {output.local_sif}) \
        $(basename {output}) \
        {params.shub_sif}
        '''
rule get_human_ref:
    output:
        # Human reference genome for removal of human DNA
        human_ref = HUMAN_REF
    message: "Downloading fasta file of human reference genome ..."
    shell:
        '''
        [ ! -d $(dirname {output.human_ref}) ] && mkdir $(dirname {output.human_ref});
        cd $(dirname {output.human_ref})
        wget https://github.com/mticlla/MetaSnk_data/raw/master/ref/hg19_main_mask_ribo_animal_allplant_allfungus.fa.gz
        '''
rule get_metaphlan_db:
    output:
        directory(METASNK_DB_DIR+'/metaphlan_databases')
    singularity:
        METAPROF_SIF
    shell:
        '''
        metaphlan2.py --install --bowtie2db {output}
        '''
rule get_strphlan_db:
    input:
        metaphlan_db_dir = METASNK_DB_DIR+'/metaphlan_databases'
    output:
        METASNK_DB_DIR+'/strainphlan_markers/all_markers.fasta'
    singularity:METAPROF_SIF
    shell:
        '''
        bowtie2-inspect {input}/mpa_v20_m200 > {output}
        '''
rule get_humann2_chocophlan:
    output:
        humann2_choco_dir = directory(METASNK_DB_DIR+'/humann2_databases/chocophlan')
    singularity:METAPROF_SIF
    shell:
        '''
        bash -c 'source activate humann2 && mkdir -p $(dirname {output.humann2_choco_dir}); \
        humann2_databases \
        --download chocophlan full $(dirname {output.humann2_choco_dir}) \
        --update-config no; \
        rm -f {output.humann2_choco_dir}/.snakemake_timestamp'
        '''
# Downloads a translated search database for HUMAnN2
# It download the EC-filtered UniRef90 database
rule get_humann2_uniref:
    output:
        humann2_uniref_dir = directory(METASNK_DB_DIR+'/humann2_databases/uniref')
    params:
        uniref_db = config['humann2prof']['get_humann2_uniref']['uniref_db']
    singularity:METAPROF_SIF
    shell:
        '''
        [ -d {output.humann2_uniref_dir} ] && rm -r {output.humann2_uniref_dir};
        bash -c 'source activate humann2 && mkdir -p $(dirname {output.humann2_uniref_dir}); \
        humann2_databases \
        --download uniref {params.uniref_db} $(dirname {output.humann2_uniref_dir}) \
        --update-config no'
        '''
rule get_humann2_utility:
    output:
        utility_mapping_dir = directory(METASNK_DB_DIR+'/humann2_databases/utility_mapping')
    singularity:METAPROF_SIF
    shell:
        '''
        bash -c 'source activate humann2 && mkdir -p $(dirname {output.utility_mapping_dir}); \
        humann2_databases \
        --download utility_mapping full $(dirname {output.utility_mapping_dir}) \
        --update-config no'
        '''
rule get_humann2_dbs:
    input:
        METASNK_DB_DIR+'/humann2_databases/chocophlan',
        METASNK_DB_DIR+'/humann2_databases/uniref',
        METASNK_DB_DIR+'/humann2_databases/utility_mapping'

rule get_panphlan_db:
    output:
        directory(METASNK_DB_DIR+'/panphlan_databases')
