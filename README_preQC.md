# MetaSnk: preQC

## Description
FastQC only performs a quality check but no QC processing is done. The **preQC**
rule runs a multi-step pre-processing of the paired fastq files, it includes:

- **trim_adapters**: adapter-trimming with "fastp". Fastp performs a quality check
  and both paired fastq files are processed as follows\:

    + remove adapters: here we provide the Nextera XT adapters,
    + base correction in overlapped regions
    + trimming of the last base in read 1
    + discard reads shorter than a minimum length, after trimming
    + a report with quality check, before and after processing
- **filter_human**: removal of reads derived from human DNA with BBTools' [bbsplit]
- **dedupe**: removal of duplicated reads with BBTools' [clumpify]
- **trim_3end**: 3\'-end quality trimming with "fastp"
- **concatenate_fastqs**: merges fastq files corresponding to the same sample into a single pair of fastq files
- **summarize_preQC**: creates summarizing tables and plots

[bbsplit]: http://seqanswers.com/forums/showthread.php?t=41288
[clumpify]: https://jgi.doe.gov/data-and-tools/bbtools/bb-tools-user-guide/clumpify-guide/

<div style="text-align:center">
  <img src="./images/preQC_dag.png"  width="350" height="500" />
</div>

## Usage

### Case 1:
From within the MetaSnk directory (i.e where it was installed, where the Snakefile is located):

    snakemake --profile ./profiles/local --directory='path/to/workdir' -n preQC

--directory : specifies the working directory and it is where snakemake will store its files for tracking the status of the workflow before/during/after execution.

After preQC is completed we can generate a html report:

    snakemake --directory='path/to/workdir' -n preQC_make_report

The report will be created in the path specified for OUT_DIR in the configuration file.

### Case2:
Execute preQC from a working directory outside the MetaSnk directory:

    snakemake --profile $metasnk/profiles/local -s $metasnk/Snakefile -n preQC
